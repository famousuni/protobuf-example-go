package main

import (
	"fmt"
	"protobuf-example-go/src/addressbook"
	"protobuf-example-go/src/helpers"
	"protobuf-example-go/src/complexpb"
	"protobuf-example-go/src/enumpb"
	"protobuf-example-go/src/simplepb"
)


func main() {
	sm := createSimple()
	helpers.WriteToFile("simple.bin", sm)
	fmt.Println()
	sm2 := &simplepb.SimpleMessage{}
	helpers.ReadFromFile("simple.bin", sm2)
	fmt.Println()
	sm2json := helpers.ToJSON(sm2)
	fmt.Println()
	sm3 := &simplepb.SimpleMessage{}
	helpers.FromJSON(sm2json, sm3)
	fmt.Println()
	createEnum()
	fmt.Println()
	createComplex()
	fmt.Println()
	//Address Book PB manipulation
	ab := addressbook.CreateAb()
	ab.AddPerson("Charlie", "Charlie@Charlie.com", "123-456-7890", 1)
	fmt.Println("Addressbook PB: ", ab)
	fmt.Println()
	ab.AddPerson("Toni", "Toni@Toni.com", "098-765-4321", 2)
	fmt.Println("Addressbook PB Update: ", ab)
	fmt.Println()
	helpers.WriteToFile("addressbook.bin", ab)
	fmt.Println()
	ab2 := &addressbook.AddressBook{}
	helpers.ReadFromFile("addressbook.bin", ab2)
}


// doSimple intanciates the simple struct from generated PB and returns
// simple pointer
func createSimple() *simplepb.SimpleMessage {
	sm := simplepb.SimpleMessage{
		Id:         12345,
		IsSimple:   true,
		Name:       "My Simple Message",
		SampleList: []int32{1, 2, 3, 4, 5},
	}
	//fmt.Printf("%+v\n", sm.Name)
	return &sm
}


func createEnum() {
	em := enumpb.EnumMessage{
		Id:           42,
		DayOfTheWeek: enumpb.DayOfTheWeek_SATURDAY,
	}
	// enum value can be set as enumpb const or pb tag
	fmt.Println("Enum PB: ", &em)
	fmt.Println()
	em.DayOfTheWeek = 3
	fmt.Println("Enum PB Update: ", &em)
}

func createComplex() {
	cm := complexpb.ComplexMessage{
		OneDummy: &complexpb.DummyMessage{
			Id:   1,
			Name: "First Message",
		},
		MultipleDummy: []*complexpb.DummyMessage{
			&complexpb.DummyMessage{
				Id:   2,
				Name: "Second Message",
			},
			&complexpb.DummyMessage{
				Id:   2,
				Name: "Third Message",
			},
		},
	}
	fmt.Println("Complex PB: ", &cm)
}
