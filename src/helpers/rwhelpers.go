package helpers

import (
	"fmt"
	"io/ioutil"
	"log"

	"github.com/golang/protobuf/jsonpb"
	"github.com/golang/protobuf/proto"
)

func WriteToFile(fname string, pb proto.Message) error {
	//Print PB before write
	fmt.Println("PB before write:\n", pb)
	out, err := proto.Marshal(pb)
	if err != nil {
		log.Fatalln("Can't serialize to bytes", err)
		return err
	}
	if err := ioutil.WriteFile(fname, out, 0644); err != nil {
		log.Fatalln("Can't write to file", err)
		return err
	}
	//fmt.Println("PB has been serialized and written")
	return nil
}

func ReadFromFile(fname string, pb proto.Message) error {
	in, err := ioutil.ReadFile(fname)
	if err != nil {
		log.Fatalln("Something went wrong when reading the file", err)
		return err
	}
	err = proto.Unmarshal(in, pb)
	if err != nil {
		log.Fatalln("Couldn't put the bytes into the protocol buffer struct", err)
	}
	//fmt.Println("PB has been unserialized and read")
	//Print PB after read
	fmt.Println("PB after read:\n", pb)
	return nil
}

func ToJSON(pb proto.Message) string {
	marshaler := jsonpb.Marshaler{}
	out, err := marshaler.MarshalToString(pb)
	if err != nil {
		log.Fatalln("Cannot convert to JSON", err)
		return ""
	}
	fmt.Println("PB after JSON Marshal:\n", out)
	return out
}

func FromJSON(in string, pb proto.Message) {
	err := jsonpb.UnmarshalString(in, pb)
	if err != nil {
		log.Fatalln("Cannot unmarshal JSON into PB", err)
		return
	}
	fmt.Println("PB after JSON Unmarshal:\n", pb)
}
