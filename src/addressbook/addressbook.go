package addressbook

// CreateAb instanciates an address book from the PB struct and returns
// a pointer to the struct
func CreateAb() *AddressBook {
	ab := AddressBook{}
	return &ab
}

// AddPerson will append a person to the people slice of the AddressBook PB
func (ab *AddressBook) AddPerson(name, email, number string, id int32) {
	ab.People = append(ab.People, &Person{
		Name:  name,
		Id:    id,
		Email: email,
		Phones: []*Person_PhoneNumber{
			&Person_PhoneNumber{
				Number: number,
				Type:   Person_MOBILE,
			},
			&Person_PhoneNumber{
				Number: number,
				Type:   Person_HOME,
			},
			&Person_PhoneNumber{
				Number: number,
				Type:   Person_WORK,
			},
		},
	},
	)
}
